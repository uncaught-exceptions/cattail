# Overview

A Wagtail website to be deployed along a Minecraft server. It acts as a
whitelist gateway with signup, a wiki, and exposes/stores some server metrics.

## Development environment

All docker commands below are run from the root of the repo.

### Build docker container

You only need to build/rebuild the container when you change requirements,
but for dev building a local container is a good idea.

```
docker build -t local/cattail_site .
```

### Run docker container for development

If you have an existing sqlite database, or media files folder, you should place
those in the `cattail_app` folder if you want them to be used.

For development you will want to mount all the app files, so that any
changes to them are reflected in the running site:
```
docker run -it --rm -p 8000:8000 --name cattail \
  -v $(pwd)/cattail_app:/opt/cattail_app \
  local/cattail_site
```

Your site should now be running on http://localhost:8000/ and you should be
able to login to http://localhost:8000/admin/

### Run docker container for testing

If you just want a running version of the site and you don't care about
using any existing data or won't need to edit the code, all you need to do is
run the container:
```
docker run -d --rm -p 8000:8000 --name cattail_site local/cattail_site
```
You can now access the site at http://localhost:8000/ and login as `admin` with
the password `adminpass`.


If you have an existing sqlite3 database and media folder you can pass those
to the container:
```
docker run -d --rm -p 8000:8000 --name cattail_site \
  -v <path_to_db>/db.sqlite3:/opt/cattail_app/db.sqlite3 \
  -v <path_to_media_folder>/media:/opt/cattail_app/media \
  local/cattail_site
```


## Config

### Django configuration

```shell
# via environment variables:
cattail_app/cattail/settings/config.py
# or settings override:
/opt/cattail_app/cattail/settings/local.py
```
`local.py` takes priority over `config.py` for any overridden variables.

See `cattail_app/cattail/settings/config.py` and the documentation
therein on how to configure the service with environmental variables.
See `cattail_app/cattail/settings/local.py.sample` for an example
of a `local.py` file.

We are assuming that Swift or S3 is always used for static file and media
storage, which may not entirely always be the case when doing local dev, but is
doable with a personal cloud user account. When `debug=True` Django will serve
it's own static and media files.


### Gunicorn configuration

```shell
/etc/cattail/conf.py
```
Details for the setting options are here:
http://docs.gunicorn.org/en/stable/settings.html

We've setup many environment variables that conf.py reads, but you can simply
override the file.

## Deployment/Upgrade

### Initial deploy

By default the container and start up checks bootstrap the service. This
includes setting up the default admin user. You can even control what the
default admin username will be via environment variables.

**IMPORTANT**: Because of how the start up checks run, when the service is intialised the *very* first time, multiple containers will cause conflicts since there is no database locking mechanism for them to use. As such the first time requires just **one** container to be started for the migrations to run, after which multiple containers are safe, and standard upgrade process can be followed.

### Upgrade

Upgrading the site is a that involves bringing down all the containers, and then bringing them back up.

Bringing up a container will run migrations and setup commands, and will lock when doing those, so bringing them all up at once is safe, but they should all be down first. We should avoid mixing old and new containers to be safe unless there are no conflicting migrations, just additive ones.
