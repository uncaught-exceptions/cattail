from django import template

from django.core.paginator import Paginator

register = template.Library()


@register.inclusion_tag(
    'news/tags/news_index_listing.html',
    takes_context=True
)
def news_index_listing(context, news_index_page):
    pages = news_index_page.get_children().live()

    news_items = sorted(
        pages, reverse=True,
        key=lambda p: p.go_live_at or p.first_published_at
        )

    paginator = Paginator(news_items, per_page=10)
    page = paginator.get_page(context['request'].GET.get('page'))

    return {
        'news_items': page,
        'news_list_style': news_index_page.news_list_style,
        'request': context['request'],
    }
