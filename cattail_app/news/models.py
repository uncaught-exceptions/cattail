
from django.db import models

from wagtail.admin.edit_handlers import FieldPanel, StreamFieldPanel

from core.pages import IndexPage, StructuredPage


class NewsIndexPage(IndexPage):
    news_list_style = models.CharField(max_length=255, choices=(
        ('title_intro_link', 'Articles with title and intro with link.'),
        ('full_no_link', 'Full articles with no link.')))

    subpage_types = ['NewsPage']

    content_panels = [
        FieldPanel('title', classname="full title"),
        FieldPanel('news_list_style', classname="News list style."),
        StreamFieldPanel('content'),
    ]


class NewsPage(StructuredPage):
    pass
