# Cattail Website

This is a [Wagtail](https://wagtail.io) application, which is ultimately a CMS
framework built on top of [Django](https://www.djangoproject.com/).

## Libraries we are using

### Django Compressor

Compresses linked and inline JavaScript or CSS into a single cached file.

As part of this we also use a precompiler for scss so that when the static
files are generated and compressed we will also build the scss directly. This
means we always work in scss, and developers never need to worry about
compiling scss to css themselves, it just works.

See more:
https://github.com/django-compressor/django-compressor
https://github.com/torchbox/django-libsass

### Django Bootstrap 3

A useful and simple library for some bootstrap html setup in the templates.

See more: https://django-bootstrap3.readthedocs.io/en/latest/

Will be switching to bootstrap3 once django library integration is finished:
https://github.com/zostera/django-bootstrap3

We don't rely on this library for CSS since we pull in the bootstrap scss so we
can override the variables in our own scss.

### Django Storages

This lets us use Swift object storage for the storage and serving of static and
media files. This means that our web server holds no actual content at all.

See more: https://github.com/dennisv/django-storage-swift

An alternative for other storage backends (S3, Azure, etc):
https://github.com/jschneier/django-storages

### Wagtail Menus

To give us a little more flexibility than using the Wagtail page structure when
it comes to building menus, and allows us to make menus for the footer and such
without ever having to hardcode any links.

See more: https://github.com/rkhleics/wagtailmenus

### Django Database Locking

For our [startup checks](#startup-checks) we needed a locking mechanism, and
rather than writing our own we're using this existing library.

See more: https://github.com/vikingco/django-db-locking/


## Running the application

See main `README.md` for project.


## Creating migration files

When you change the python code and need to create new migrations (or check if
new ones are needed), you'll want to run the makemigrations command through the
docker container environment.

First follow the build step in the main `README.md` file under the section:
**Development environment** > **Build docker container**

Once you have a local container built, all you need to do is run the
following command from the repo root folder:
```
docker run -it --rm -p 8000:8000 --name cattail_site \
  -v $(pwd)/cattail_app:/opt/cattail_app \
  local/cattail_site \
  --django-manage makemigrations
```

**Note**: These migrations files will be owned by 'root' because they were
created within the scope of the container (using the container's root user).
You may want to chown them (or your whole repo folder), but git doesn't really
seem to mind, but if you notice any odd permissions issues around these files,
this may be why.


## Startup checks

To facilitate easier upgrades and container rollovers we wanted each container
to be safe to start up on it's own, and attempt to handle new migrations (if
needed) and collection of static files.

The code for this is located in this same folder as `startup_check.py`.

This is run on every container start up and simply ensures that the database is
in the state the container needs it to be to run.

## Theming

### Styling and compiling scss files

* `core.scss` is equivalent to `style(s).scss`
* After running `python manage.py runserver`, any changes in scss files will
  be compiled automatically.
* Refresh the page
