from django.db import models
from django.utils.translation import ugettext_lazy as _

from modelcluster.fields import ParentalKey
from wagtail.admin.edit_handlers import FieldPanel, PageChooserPanel
from wagtailmenus.models import AbstractFlatMenuItem


class CustomFlatMenuItem(AbstractFlatMenuItem):
    """A custom menu item model to be used by ``CustomFlatMenu``

    Has two additonal fields for style (header-menu only) and
    icons (footer-menu only).
    """

    menu = ParentalKey(
        'wagtailmenus.FlatMenu',
        on_delete=models.CASCADE,
        related_name="custom_menu_items",
    )
    footer_icon = models.CharField(
        verbose_name=_("Footer Item Icon"),
        max_length=255,
        blank=True,
        help_text=_(
            "Icon to put next to the menu item when in a 'footer-menu'. "
            "This needs to be a fontawesome icon name like 'twitter', "
            "'github', or 'envelope-o'. See: "
            "http://fontawesome.io/icons/"
        )
    )

    panels = (
        PageChooserPanel("link_page"),
        FieldPanel("link_url"),
        FieldPanel("url_append"),
        FieldPanel("link_text"),
        FieldPanel("footer_icon"),
        FieldPanel("handle"),
        FieldPanel("allow_subnav"),
    )
