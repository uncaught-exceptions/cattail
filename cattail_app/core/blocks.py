from wagtail.contrib.table_block.blocks import TableBlock
from wagtail.core.blocks import (
    TextBlock, StructBlock, StreamBlock, CharBlock, RichTextBlock,
    RawHTMLBlock, ChoiceBlock, PageChooserBlock, IntegerBlock, ListBlock)
from wagtail.embeds.blocks import EmbedBlock
from wagtail.images.blocks import ImageChooserBlock


header_styles = [
    ('h2', 'H2'),
    ('h3', 'H3'),
    ('h4', 'H4'),
    ('h5', 'H5'),
]


class HeaderBlock(StructBlock):
    text = CharBlock()
    style = ChoiceBlock(
        label="Header Style", choices=header_styles)

    class Meta:
        icon = "title"
        template = 'core/blocks/header_block.html'


text_styles = [
    ('', 'Unstyled'),
    ('no-spacing', 'No spacing'),
    ('well', 'Well'),
]


class StyledTextBlock(StructBlock):
    text = RichTextBlock()
    style = ChoiceBlock(
        label="Text Style", choices=text_styles,
        required=False)

    class Meta:
        icon = "pilcrow"
        template = 'core/blocks/styled_text_block.html'


class ImageBlock(StructBlock):
    image = ImageChooserBlock()
    caption = TextBlock(required=False)

    class Meta:
        icon = "image/picture"
        template = 'core/blocks/image_block.html'


class ImageCarouselBlock(StructBlock):
    title = CharBlock(max_length=100, label="Title", required=False)
    description = TextBlock(
        max_length=200, label="Description", required=False)
    image_ratio = ChoiceBlock(
        label="Image Ratio", choices=[("2:1", "2:1"), ("3:2", "3:2")],
        default="2:1")
    images = ListBlock(ImageBlock())

    class Meta:
        icon = "image"
        template = 'core/blocks/image_carousel_block.html'


class GalleryBlock(StructBlock):
    gallery = PageChooserBlock(target_model="gallery.GalleryPage")
    number_of_images = IntegerBlock(default=4, min_value=4, max_value=20)

    class Meta:
        icon = "image"
        template = 'core/blocks/gallery_block.html'


class SelectedImagesBlock(StructBlock):
    images = ListBlock(ImageBlock())

    class Meta:
        icon = "image"
        template = 'core/blocks/selected_images_block.html'


class StructuredStreamBlock(StreamBlock):
    header = HeaderBlock(icon="title")
    text = StyledTextBlock()
    image_carousel = ImageCarouselBlock()
    raw_html = RawHTMLBlock(label='Raw HTML')
    video = EmbedBlock()
    table = TableBlock(template='core/blocks/table_block.html')
    gallery = GalleryBlock()
    images = SelectedImagesBlock()

    class Meta:
        template = 'core/blocks/structured_stream_block.html'
