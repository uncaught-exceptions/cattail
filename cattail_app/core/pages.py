from django.db import models
from django.shortcuts import redirect
from django.conf import settings
from django.core.exceptions import ValidationError
from django.utils.text import slugify
from django.core.mail import EmailMultiAlternatives

from wagtail.core.fields import StreamField
from wagtail.contrib.forms.edit_handlers import FormSubmissionsPanel
from wagtail.admin.edit_handlers import (
    FieldPanel, MultiFieldPanel, FieldRowPanel, StreamFieldPanel, InlinePanel)
from wagtail.contrib.forms.models import AbstractFormField
from wagtail.search import index
from wagtail.images.edit_handlers import ImageChooserPanel

from wagtailcaptcha.forms import WagtailCaptchaFormBuilder
from wagtailcaptcha.models import WagtailCaptchaEmailForm

from wagtailmenus.models import MenuPage

from modelcluster.fields import ParentalKey

from core import blocks
from core import fields


class BasePage(MenuPage):
    show_title = models.BooleanField(
        default=True,
        help_text=(
            "Show title at the top of the page."))
    linkable = models.BooleanField(
        default=True,
        help_text=(
            "Is this page linkable in the menu "
            "(False makes it just a header)."))

    index_image = models.ForeignKey(
        'wagtailimages.Image',
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        related_name='+'
    )

    promote_panels = MenuPage.promote_panels + [
        FieldPanel('show_title'),
        FieldPanel('linkable'),
        ImageChooserPanel('index_image'),
    ]

    class Meta:
        abstract = True


class RedirectPage(BasePage, fields.LinkFields):

    def serve(self, request):
        if self.link:
            return redirect(self.link, permanent=False)
        else:
            return redirect("/", permanent=False)

    content_panels = [FieldPanel('title')] + fields.LinkFields.panels


# StructuredPage

class StructuredPage(BasePage):
    content = StreamField(blocks.StructuredStreamBlock)

    search_fields = BasePage.search_fields + [
        index.SearchField('content'),
    ]

    content_panels = [
        FieldPanel('title', classname="full title"),
        StreamFieldPanel('content'),
    ]


# Index page

class IndexPage(BasePage):
    sorting_method = models.CharField(
        max_length=6,
        choices=(
            ('manual', 'Manually sorted'),
            ('newest', 'Newest first'),
            ('oldest', 'Oldest first'),
        ),
        default='manual',
        help_text="The way the index list items are sorted.",
    )
    content = StreamField(blocks.StructuredStreamBlock, blank=True)

    search_fields = BasePage.search_fields + [
        index.SearchField('content'),
    ]
    content_panels = [
        FieldPanel('title', classname="full title"),
        FieldPanel('sorting_method'),
        StreamFieldPanel('content'),
    ]


class SafeCaptchaFormBuilder(WagtailCaptchaFormBuilder):

    @property
    def formfields(self):
        fields = super(SafeCaptchaFormBuilder, self).formfields
        if not settings.RECAPTCHA_PUBLIC_KEY:
            fields.pop(self.CAPTCHA_FIELD_NAME)
        return fields


class FormField(AbstractFormField):
    page = ParentalKey('FormPage', related_name='form_fields')


class FormPage(BasePage, WagtailCaptchaEmailForm):
    intro = StreamField(blocks.StructuredStreamBlock)
    thank_you_text = StreamField(blocks.StructuredStreamBlock)
    reply_to_from_field = models.CharField(
        max_length=255, blank=True,
        help_text=(
            "Label of the form field to get reply-to from. "
            "Supercedes From Address.")
    )

    content_panels = [
        FormSubmissionsPanel(),
        FieldPanel('title', classname="full title"),
        StreamFieldPanel('intro'),
        InlinePanel(
            'form_fields', label="Form fields"),
        StreamFieldPanel('thank_you_text'),
        MultiFieldPanel([
            FieldPanel('to_address'),
            FieldRowPanel([
                FieldPanel('reply_to_from_field', classname="col6"),
                FieldPanel('from_address', classname="col6"),
            ]),
            FieldPanel('subject'),
        ], "Email"),
    ]

    def __init__(self, *args, **kwargs):
        super(FormPage, self).__init__(*args, **kwargs)
        self.form_builder = SafeCaptchaFormBuilder

    def clean(self):
        super(FormPage, self).clean()

        if self.reply_to_from_field:
            reply_field = str(slugify(self.reply_to_from_field))
            found = False
            is_email = False
            for field in self.form_fields.all():
                if field.clean_name == reply_field:
                    found = True
                    if field.field_type == "email":
                        is_email = True

            if not found:
                raise ValidationError(
                    {'reply_to_from_field': (
                        'Form field with this label must exist.')}
                )
            if not is_email:
                raise ValidationError(
                    {'reply_to_from_field': (
                        'Form field with this label must be an email field.')}
                )

    def send_mail(self, form):
        addresses = [x.strip() for x in self.to_address.split(',')]
        content = []
        reply_field = str(slugify(self.reply_to_from_field))
        from_address = None
        for field in form:
            value = field.value()
            if isinstance(value, list):
                value = ', '.join(value)
            content.append('{}: {}'.format(field.label, value))
            if str(slugify(field.label)) == reply_field:
                from_address = value
        content = '\n'.join(content)

        headers = {}
        if from_address:
            headers['Reply-To'] = from_address

        email = EmailMultiAlternatives(
            self.subject,
            content,
            self.from_address,
            addresses,
            headers=headers,
        )

        email.send(fail_silently=False)
