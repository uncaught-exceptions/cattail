import uuid

from django import template
from django.conf import settings

from wagtail.core.models import Page

from theme.models import ThemeSettings

register = template.Library()


@register.filter
def get_class(obj):
    return obj.__class__.__name__


@register.simple_tag()
def random_id():
    """
    Create a random uuid4 based id to use for a div id.
    """
    return uuid.uuid4().hex


def get_index_listing(context, calling_page):
    pages = calling_page.get_children().live()

    if calling_page.sorting_method == 'newest':
        sorted_pages = sorted(
            pages, reverse=True,
            key=lambda p: p.go_live_at or p.first_published_at
            )
        pages = sorted_pages
    elif calling_page.sorting_method == 'oldest':
        sorted_pages = sorted(
            pages, reverse=False,
            key=lambda p: p.go_live_at or p.first_published_at
            )
        pages = sorted_pages

    return {
        'pages': pages,
        # required by the pageurl tag that we want to use within this template
        'request': context['request'],
    }


# Retrieves all live pages. which are children of the calling page
# for standard index listing
@register.inclusion_tag(
    'core/tags/article_index_listing.html',
    takes_context=True
)
def article_index_listing(context, calling_page):
    return get_index_listing(context, calling_page)


@register.inclusion_tag('core/tags/breadcrumbs.html', takes_context=True)
def breadcrumbs(context):
    self = context.get('self')
    if self is None or self.depth <= 2:
        # When on the home page, displaying breadcrumbs is irrelevant.
        ancestors = ()
    else:
        ancestors = Page.objects.ancestor_of(
            self, inclusive=True).filter(depth__gt=2)
    return {
        'ancestors': ancestors,
        'request': context['request'],
    }


@register.inclusion_tag(
    'core/tags/google_analytics.html',
    takes_context=True,
)
def google_analytics(context):
    ga_track_id = getattr(settings, 'GOOGLE_ANALYTICS_TRACKING_ID', False)

    theme_settings = ThemeSettings.for_site(context['request'].site)
    if theme_settings.google_analytics_id:
        ga_track_id = theme_settings.google_analytics_id

    if ga_track_id:
        return {
            'GOOGLE_ANALYTICS_TRACKING_ID': ga_track_id,
        }
    return {}
