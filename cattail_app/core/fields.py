
from django.db import models
from django.utils.timezone import now

from wagtail.core.fields import RichTextField
from wagtail.admin.edit_handlers import (
    FieldPanel, PageChooserPanel)
from wagtail.documents.edit_handlers import DocumentChooserPanel


# A couple of abstract classes that contain commonly used fields

class LinkFields(models.Model):
    link_page = models.ForeignKey(
        'wagtailcore.Page',
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        related_name='+'
    )
    link_document = models.ForeignKey(
        'wagtaildocs.Document',
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        related_name='+'
    )
    link_external = models.URLField("External link", blank=True)

    @property
    def link(self):
        if self.link_page:
            return self.link_page.url
        elif self.link_document:
            return self.link_document.url
        else:
            return self.link_external

    panels = [
        PageChooserPanel('link_page'),
        DocumentChooserPanel('link_document'),
        FieldPanel('link_external'),
    ]

    class Meta:
        abstract = True


class NoteFields(models.Model):
    note = RichTextField()
    image = models.ForeignKey(
        'wagtailimages.Image',
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        related_name='+'
    )
    document = models.ForeignKey(
        'wagtaildocs.Document',
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        related_name='+'
    )
    noted_at = models.DateTimeField(default=now)

    panels = [
        FieldPanel('note'),
        FieldPanel('noted_at'),
    ]

    class Meta:
        abstract = True
