from copy import copy

from cattail.exceptions import MissingConfigurationException


def parse_boolean(value):
    if type(value) == bool:
        return value

    if value in ['True', 'true', 'T', 't', '1']:
        return True
    if value in ['False', 'false', 'F', 'f', '0']:
        return False
    return False


def parse_val_or_none(value):
    if not value or value in ['None', 'none', 'Null', 'null']:
        return None
    return value


def ensure_config(value, err_msg=None):
    if value is None:
        raise MissingConfigurationException(message=err_msg)
    return value


def remove_field_panels(panel_group, fields):
    cleaned_panels = []
    for panel in panel_group:
        field_name = getattr(panel, 'field_name', None)
        if field_name and field_name not in fields:
            cleaned_panels.append(panel)

        child_panels = getattr(panel, 'children', None)
        if not field_name and child_panels:
            copied_panel = copy(panel)
            copied_panel.children = remove_field_panels(child_panels, fields)
            cleaned_panels.append(copied_panel)
    return cleaned_panels
