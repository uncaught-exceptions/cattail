from django.core.files.storage import get_storage_class
from django.conf import settings

from storages.backends.s3boto3 import S3Boto3Storage


class S3Storage(S3Boto3Storage):
    """
    S3 storage backend sets CORS.
    """

    def _get_or_create_bucket(self, name, cors=None):
        bucket = super()._get_or_create_bucket(name)
        allowed_origin = settings.AWS_AUTO_CREATE_BUCKET_ALLOW_ORIGINS
        if allowed_origin:
            cors_config = {
                'CORSRules': [
                    {
                        'AllowedMethods': ['GET'],
                        'AllowedOrigins': allowed_origin.split(',')
                    }
                ]
            }
            bucket.Cors().put(CORSConfiguration=cors_config)
        return bucket


class LocalCachedS3StaticStorage(S3Storage):
    """
    S3 storage backend that saves the files locally for compression
    and has a separate bucket.
    """

    def __init__(self, *args, **kwargs):
        self.bucket_name = (
            getattr(settings, 'AWS_STATIC_STORAGE_BUCKET_NAME') or
            getattr(settings, 'AWS_STORAGE_BUCKET_NAME'))
        self.auto_create_bucket = (
            getattr(settings, 'AWS_STATIC_AUTO_CREATE_BUCKET') or
            getattr(settings, 'AWS_AUTO_CREATE_BUCKET', False))
        self.default_acl = (
            getattr(settings, 'AWS_STATIC_DEFAULT_ACL') or
            getattr(settings, 'AWS_DEFAULT_ACL', 'public-read'))
        self.bucket_acl = (
            getattr(settings, 'AWS_STATIC_BUCKET_ACL') or
            getattr(settings, 'AWS_BUCKET_ACL', self.default_acl))
        self.querystring_auth = (
            getattr(settings, 'AWS_STATIC_QUERYSTRING_AUTH') or
            getattr(settings, 'AWS_QUERYSTRING_AUTH', True))
        self.querystring_expire = (
            getattr(settings, 'AWS_STATIC_QUERYSTRING_EXPIRE') or
            getattr(settings, 'AWS_QUERYSTRING_EXPIRE', 3600))
        self.signature_version = (
            getattr(settings, 'AWS_STATIC_S3_SIGNATURE_VERSION') or
            getattr(settings, 'AWS_S3_SIGNATURE_VERSION'))
        super(LocalCachedS3StaticStorage, self).__init__(*args, **kwargs)
        self.local_storage = get_storage_class(
            "compressor.storage.CompressorFileStorage")()

    def save(self, name, content):
        self.local_storage._save(name, content)

        if not name.endswith(tuple(settings.ONLY_LOCAL_STATIC_FILE_TYPES)):
            super(LocalCachedS3StaticStorage, self).save(
                name, self.local_storage._open(name))
        return name
