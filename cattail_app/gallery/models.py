from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.db import models
from django.utils.translation import ugettext_lazy as _

from wagtail.admin.edit_handlers import FieldPanel
from wagtail.core.fields import RichTextField
from wagtail.images.edit_handlers import ImageChooserPanel

from cattail.utils import remove_field_panels
from core.pages import BasePage

from gallery.utils import get_gallery_images


IMAGE_ORDER_TYPES = (
    (1, 'Image title (with "[<number>]" in the title)'),
    (2, 'Newest image first'),
)


class GalleryIndexPage(BasePage):
    description = RichTextField(
        blank=True,
        help_text=_('Optional description text to go with the galleries.')
    )
    galleries_per_page = models.IntegerField(
        default=15,
        verbose_name=_('Galleries per page'),
        help_text=_('How many galleries there should be on one page.')
    )

    subpage_types = ['GalleryPage']

    content_panels = [
        FieldPanel('title', classname="full title"),
        FieldPanel('description', classname="full"),
        FieldPanel('galleries_per_page', classname='full title'),
    ]


class GalleryPage(BasePage):
    description = RichTextField(
        blank=True,
        help_text=_('Optional description text to go with the gallery.')
    )
    collection = models.ForeignKey(
        'wagtailcore.Collection',
        verbose_name=_('Collection'),
        null=True,
        blank=False,
        on_delete=models.SET_NULL,
        related_name='+',
        help_text=_('Show images in this collection in the gallery view.')
    )
    images_per_page = models.IntegerField(
        default=24,
        verbose_name=_('Images per page'),
        help_text=_('How many images there should be on one page.')
    )
    use_lightbox = models.BooleanField(
        verbose_name=_('Use lightbox'),
        default=True,
        help_text=_(
            'Use lightbox to view larger images when clicking the thumbnail.')
    )
    order_images_by = models.IntegerField(choices=IMAGE_ORDER_TYPES, default=1)

    content_panels = BasePage.content_panels + [
        FieldPanel('description', classname='full'),
        ImageChooserPanel('index_image'),
        FieldPanel('collection'),
        FieldPanel('images_per_page', classname='full title'),
        FieldPanel('use_lightbox'),
        FieldPanel('order_images_by'),
    ]

    promote_panels = remove_field_panels(
        BasePage.promote_panels, ['index_image'])

    @property
    def images(self):
        return get_gallery_images(self.collection.name, self)

    def get_context(self, request):
        images = self.images
        page = request.GET.get('page')
        paginator = Paginator(images, self.images_per_page)
        try:
            images = paginator.page(page)
        except PageNotAnInteger:
            images = paginator.page(1)
        except EmptyPage:
            images = paginator.page(paginator.num_pages)
        context = super(GalleryPage, self).get_context(request)
        context['gallery_images'] = images
        context['paginate'] = True
        return context
