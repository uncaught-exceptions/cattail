import re

from django import template

from django.core.paginator import Paginator

from gallery.utils import get_gallery_images

register = template.Library()


@register.inclusion_tag(
    'gallery/tags/gallery_index_listing.html',
    takes_context=True
)
def gallery_index_listing(context, gallery_index_page):
    galleries = gallery_index_page.get_children().live()

    paginator = Paginator(
        galleries, per_page=gallery_index_page.galleries_per_page)
    page = paginator.get_page(context['request'].GET.get('page'))

    return {
        'galleries': page,
        'request': context['request'],
    }


@register.inclusion_tag('gallery/tags/simple_gallery.html')
def simple_gallery(collection=None, images=None, tags=None, image_limit=None,
                   use_lightbox=True):
    if not collection and not images:
        return

    if not images:
        images = get_gallery_images(
            collection, tags=tags.split() if tags else None)
    if image_limit:
        images = images[:int(image_limit)]
    return {
        'gallery_images': images,
        'use_lightbox': use_lightbox,
        'paginate': False
    }


@register.filter
def hide_num_order(title):
    number_match = re.match(r'^.*?\[[^\d]*(\d+)[^\d]*\].*$', title)
    if number_match:
        number = number_match.group(1)
        return title.replace('[{}]'.format(number), '')
    return title
