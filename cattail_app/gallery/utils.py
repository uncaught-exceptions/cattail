
from wagtail.images.models import Image


def get_gallery_images(collection, page=None, tags=None):
    images = Image.objects.filter(collection__name=collection)
    try:
        if page:
            if page.order_images_by == 0:
                images = images.order_by('title')
            elif page.order_images_by == 1:
                images = images.order_by('-created_at')
    except AttributeError:
        pass

    if images and tags:
        images = images.filter(tags__name__in=tags).distinct()
    return images
