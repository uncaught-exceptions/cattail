from django.db import models

from wagtail.contrib.settings.models import BaseSetting, register_setting
from wagtail.admin.edit_handlers import FieldPanel

from core.pages import * # noqa
from core.menus import * # noqa


@register_setting(icon='placeholder')
class ThemeSettings(BaseSetting):
    class Meta:
        verbose_name = 'theme settings'

    google_analytics_id = models.CharField(
        blank=True, max_length=255,
        help_text="Unique code to use for this particular site.")
    bookmark_icon = models.ImageField(blank=True)
    header_logo = models.ImageField(blank=True)
    menu_style_classes = models.CharField(
        blank=True, max_length=255,
        help_text="Style classes for the navbar. Based on Bootstrap 4, e.g. "
                  "'navbar-light' or 'navbar-dark' and 'bg-light', 'bg-dark' "
                  "or any other Bootstrap 4 'bg-*' option.")
    show_search = models.BooleanField(
        default=True, help_text="Show search in menu.")
    show_breadcrumbs = models.BooleanField(
        default=True, help_text="Show breadcrumbs under menu.")
    alternative_juveniles_text = models.CharField(
        blank=True, max_length=255,
        help_text="Alternative text for 'Juveniles', e.g. 'Kittens' or "
                  "'Puppies'.")
    extra_theme_css = models.TextField(
        blank=True,
        help_text="Extra CSS to be inserted into a <style> "
                  "element in the <head> element after all other CSS.")
    raw_head_html = models.TextField(
        blank=True,
        help_text="Raw HTML that will be inserted into the <head>. Useful for"
                  "introducing extra fonts or other external JS.")
    raw_footer_html = models.TextField(
        blank=True,
        help_text="Raw HTML that will be inserted into the footer. Useful for"
                  "copyright notice, or affiliate links.")

    panels = [
        FieldPanel('google_analytics_id'),
        FieldPanel('bookmark_icon'),
        FieldPanel('header_logo'),
        FieldPanel('menu_style_classes'),
        FieldPanel('show_search'),
        FieldPanel('show_breadcrumbs'),
        FieldPanel('alternative_juveniles_text'),
        FieldPanel('extra_theme_css'),
        FieldPanel('raw_head_html'),
        FieldPanel('raw_footer_html'),
    ]
