from django import template

from core.templatetags.core_tags import get_index_listing


register = template.Library()


@register.inclusion_tag(
    'animal/tags/animal_detail_index_listing.html',
    takes_context=True
)
def animal_detail_index_listing(context, calling_page):
    new_context = get_index_listing(context, calling_page)
    new_pages = []
    for page in new_context['pages']:
        try:
            if page.specific.animal_page:
                new_pages.append(page.specific.animal_page)
        except AttributeError:
            new_pages.append(page)
    new_context['pages'] = new_pages
    return new_context
