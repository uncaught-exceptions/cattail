from django.db import models
from django.shortcuts import redirect

from django.utils.timezone import now

from wagtail.core.models import Orderable
from wagtail.core.fields import StreamField
from wagtail.admin.edit_handlers import (
    FieldPanel, MultiFieldPanel, PageChooserPanel, InlinePanel,
    StreamFieldPanel)
from wagtail.images.edit_handlers import ImageChooserPanel
from wagtail.search import index

from modelcluster.fields import ParentalKey

from core import fields, blocks
from core.pages import BasePage, IndexPage


# Animal Detail Index Page

class AnimalDetailIndexPage(IndexPage):
    subpage_types = ['AnimalDetailPage', 'AnimalDetailDuplicatePage']


# Animal Details page

class AnimalDetailSireLink(fields.LinkFields):
    name = models.CharField(max_length=255)
    link_page = models.ForeignKey(
        'animal.AnimalDetailPage',
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        related_name='+'
    )
    page = ParentalKey(
        'animal.AnimalDetailPage',
        related_name='sire_link',
        unique=True
    )

    panels = [FieldPanel('name'), ] + fields.LinkFields.panels


class AnimalDetailDamLink(fields.LinkFields):
    name = models.CharField(max_length=255)
    link_page = models.ForeignKey(
        'animal.AnimalDetailPage',
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        related_name='+'
    )
    page = ParentalKey(
        'animal.AnimalDetailPage',
        related_name='dam_link',
        unique=True
    )
    panels = [FieldPanel('name'), ] + fields.LinkFields.panels


class AnimalDetailPedigreeLink(fields.LinkFields):
    page = ParentalKey(
        'animal.AnimalDetailPage',
        related_name='pedigree_link',
        unique=True
    )


class AnimalDetailPageNotes(Orderable, fields.NoteFields):
    page = ParentalKey('animal.AnimalDetailPage', related_name='notes')


class AnimalDetailPage(BasePage):
    name = models.CharField(max_length=255)
    breed = models.CharField(max_length=255, blank=True)
    sex = models.CharField(max_length=255, choices=(
        ('Male', 'Male'), ('Female', 'Female')))
    colour = models.CharField(max_length=255, blank=True)
    date_of_birth = models.DateField()
    extra_info = StreamField(blocks.StructuredStreamBlock, blank=True)
    image = models.ForeignKey(
        'wagtailimages.Image',
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        related_name='+'
    )
    gallery = models.ForeignKey(
        'gallery.GalleryPage',
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        related_name='+'
    )

    search_fields = BasePage.search_fields + [
        index.SearchField('name'),
        index.FilterField('breed'),
        index.FilterField('sex'),
        index.FilterField('colouring'),
        index.FilterField('date_of_birth'),
        index.SearchField('extra_info'),
    ]

    @property
    def sire(self):
        return self.sire_link.all().first()

    @property
    def dam(self):
        return self.dam_link.all().first()

    @property
    def pedigree(self):
        return self.pedigree_link.all().first()

    @property
    def litters(self):
        litters = []
        now_date = now().date()
        if self.sex == "Male":
            for litter in self.litters_sire.all():
                if litter.date_of_birth and litter.date_of_birth <= now_date:
                    litters.append(litter)
        elif self.sex == "Female":
            for litter in self.litters_dam.all():
                if litter.date_of_birth and litter.date_of_birth <= now_date:
                    litters.append(litter)
        return litters

    content_panels = [
        FieldPanel('title', classname="full title"),
        ImageChooserPanel('image'),
        PageChooserPanel('gallery', 'gallery.GalleryPage'),
        MultiFieldPanel(
            [
                FieldPanel('name'),
                FieldPanel('sex'),
                FieldPanel('breed'),
                FieldPanel('colour'),
                FieldPanel('date_of_birth'),
            ], "Details"),
        InlinePanel('sire_link', label="Sire"),
        InlinePanel('dam_link', label="Dam"),
        InlinePanel('pedigree_link', label="Pedigree"),
        StreamFieldPanel('extra_info'),
        InlinePanel('notes', label="Notes (not publically visible)"),
    ]


class AnimalDetailDuplicatePage(BasePage):
    animal_page = models.ForeignKey(
        'animal.AnimalDetailPage',
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        related_name='+'
    )

    def serve(self, request):
        if self.animal_page:
            return redirect(
                self.animal_page.relative_url(
                    request.site, request),
                permanent=False)
        else:
            return redirect("/", permanent=False)

    content_panels = [
        FieldPanel('title', classname="full title"),
        PageChooserPanel('animal_page', 'animal.AnimalDetailPage'),
    ]
