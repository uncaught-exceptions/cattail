from django import template

from core.templatetags.core_tags import get_index_listing


register = template.Library()


@register.inclusion_tag(
    'litter/tags/animal_litter_index_listing.html',
    takes_context=True
)
def animal_litter_index_listing(context, calling_page):
    return get_index_listing(context, calling_page)
