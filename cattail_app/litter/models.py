
from django.db import models

from wagtail.core.models import Orderable
from wagtail.core.fields import RichTextField, StreamField
from wagtail.admin.edit_handlers import (
    FieldPanel, MultiFieldPanel, PageChooserPanel, InlinePanel,
    StreamFieldPanel)
from wagtail.images.edit_handlers import ImageChooserPanel
from wagtail.search import index

from modelcluster.fields import ParentalKey

from core import fields, blocks
from core.pages import BasePage, IndexPage


# Animal Detail Index Page

class AnimalLitterIndexPage(IndexPage):
    subpage_types = ['AnimalLitterPage']


# Animal Litter page

class Juvenile(Orderable, models.Model):
    name = models.CharField(max_length=255)
    sex = models.CharField(max_length=255, choices=(
        ('Male', 'Male'), ('Female', 'Female')))
    colour = models.CharField(max_length=255)
    birth_weight = models.CharField(max_length=255)
    status = models.CharField(max_length=255)
    gallery = models.ForeignKey(
        'gallery.GalleryPage',
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        related_name='+'
    )
    extra_info = RichTextField(blank=True)
    image = models.ForeignKey(
        'wagtailimages.Image',
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        related_name='+'
    )
    parent = ParentalKey('litter.AnimalLitterPage', related_name='juveniles')
    detail_page = models.ForeignKey(
        'animal.AnimalDetailPage',
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        related_name='+'
    )

    @property
    def link(self):
        return self.detail_page.url

    panels = [
        MultiFieldPanel(
            [
                FieldPanel('name'),
                FieldPanel('sex'),
                FieldPanel('colour'),
                FieldPanel('birth_weight'),
                FieldPanel('status'),
            ], "Details"),
        PageChooserPanel('gallery', 'gallery.GalleryPage'),
        FieldPanel('extra_info', classname="full"),
        ImageChooserPanel('image'),
        PageChooserPanel('detail_page'),
    ]


class AnimalLitterPedigreeLink(fields.LinkFields):
    page = ParentalKey(
        'litter.AnimalLitterPage',
        related_name='pedigree_link',
        unique=True
    )


class AnimalLitterPageNotes(Orderable, fields.NoteFields):
    page = ParentalKey('litter.AnimalLitterPage', related_name='notes')


class AnimalLitterPage(BasePage):
    sire = models.ForeignKey(
        'animal.AnimalDetailPage',
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        related_name='litters_sire'
    )
    dam = models.ForeignKey(
        'animal.AnimalDetailPage',
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        related_name='litters_dam'
    )
    gallery = models.ForeignKey(
        'wagtailcore.Page',
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        related_name='+'
    )
    date_of_birth = models.DateField(null=True, blank=True)
    extra_info = StreamField(blocks.StructuredStreamBlock, blank=True)

    search_fields = BasePage.search_fields + [
        index.SearchField('extra_info'),
    ]

    @property
    def pedigree(self):
        return self.pedigree_link.all().first()

    content_panels = [
        FieldPanel('title', classname="full title"),
        PageChooserPanel('sire'),
        PageChooserPanel('dam'),
        FieldPanel('date_of_birth'),
        InlinePanel('pedigree_link', label="Pedigree"),
        PageChooserPanel('gallery', 'gallery.GalleryPage'),
        StreamFieldPanel('extra_info'),
        InlinePanel('juveniles', label="Juveniles"),
        InlinePanel('notes', label="Notes (not publically visible)"),
    ]
