FROM python:alpine3.6
COPY entrypoint.sh /opt/
COPY cattail_app /opt/cattail_app
COPY etc/conf.py /etc/cattail/
RUN apk add --no-cache \
      bash \
      gcc \
      curl \
      g++ \
      libstdc++ \
      linux-headers \
      musl-dev \
      postgresql-dev \
      mariadb-dev \
      jpeg-dev \
      zlib-dev \
      libmagic && \
    pip install --upgrade pip && \
    pip install -r /opt/cattail_app/requirements.txt && \
    pip install gunicorn;
HEALTHCHECK --interval=2s --timeout=5s --retries=5 \
   CMD curl -f http://localhost:8000/ || exit 1
EXPOSE 8000
WORKDIR /opt
ENTRYPOINT ["/opt/entrypoint.sh"]
CMD ["--start-service"]
